.. pop-config documentation master file, created by
   sphinx-quickstart on Wed Mar 11 13:52:53 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pop-config's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :glob:

   topics/quickstart
   topics/config
   topics/cli_config


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

==========
CLI_CONFIG
==========

The heaviest configuration options will be in the *CLI_CONFIG* dictionary.
All options that appear on the CLI need to be activated in the *CLI_CONFIG*
but the basic configuration needs to be in the *CONFIG* dictionary.

Pop-config uses Python's venerable `argparse` under the hood to present
and process the arguments. Pop-config will also, transparently, pass
options from the dictionary into `argparse`, this makes Pop-config
transparently compatible with new `argparse` options that are made available.

This document is intended, therefore, to present the most commonly used
options, please see the `argparse` doc for more in depth data on available
options.

If the cli option is very simple it can be as simple as just doing this:

.. code-block:: python

    CLI_CONFIG = {
        "test": {},
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This way the options presented in the cli are all explicitly set. But
additional options about how the cli is presented to the end user is easy to do.

This document will cover all available options for the *CLI_CONFIG*,
remember that the `default` and `help` values should always be in the *CONFIG*
section to facilitate easy app-merging of configuration data and settings.

Source
======

By default the *CLI_CONFIG* references the local *CONFIG* setting. The `source`
option allows you to reference a documented configuration from a separate
project configuration. This powerful option allows you to manage the arguments
and flags in a namespace of an app that is being merged into this app. The
benefit here is that the *CONFIG* values do not need to be rewritten and you
maintain a single authoritative source of documentation.

When using `source` in the *CLI_CONFIG* the namespace that defined the option
in the *CONFIG* dictionary will own the option. This makes it easy to have
an application that uses its own config namespace be app merged into another
application that can then transparently manage the configuration of the
merged app.

Dyne
====

A powerful option in the *CLI_CONFIG* is `dyne`. This uses vertical app
merging to modify another application's cli options. This allows a vertical
app merge repo to define cli arguments that will be made available when the
plugins are installed to extend an external app.

Options
=======

By default the options presented on the command line are identical to the
name of the value. So for the above example the presented option would be
`--test`. If alternative options are desired, they can be easily added:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "options": ["-t", "--testy-mc-tester", "-Q"],
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Pop-config automatically determines between short options and long options.

Positional Arguments
====================

Positional arguments are very common and can create a much more user friendly
experience for users. Adding positional arguments are easy. Just use the
`positional` argument:


.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "positional": True,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

When working with multiple positional arguments the `display_priority` flag can be
used:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "positional": True,
            "display_priority": 2,
        },
        "run": {
            "positional": True,
            "display_priority": 1,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
        "run": {
            "default": "green",
            "help": "What color to run",
        },
    }

In the above example the first argument will be `run` and the second will be `test`.

Accepting Environment Variables
===============================

Operating systems allow for configuration options to be passed in via
specific means. In Unix based systems like Linux and MacOS, environment variables
can be used. In Windows based systems the registry can be used. To allow
for an os variable to be used just add the os option:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "os": "MYAPP_TEST",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Now the flag can be set by setting the environment variable `MYAPP_TEST` to
the desired configuration value.

Actions
=======

Actions allow a command line argument to perform an action, or flip a switch.

The `action` option passes through to `argparse`, if the examples in this
document do not make sense you can also check the `argsparse` section on
`action`.

A number of actions are supported by `argparse`. Arguable the most frequently
used actions are `store_true` and `store_false`:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "action": "store_true",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

A few other useful actions are `append` and `count`. If `append` is used then
every time the argument is used the option passed to the argument is appended
to the final list. The `count` option allows for the number of times that the
argument is passed to be counted up. This is useful for situations where you
want to specify what the verbosity of the output should be, so that you can
pass `-vvv` in a similar fashion to ssh.

Number of Arguments
===================

The number of arguments that should be expected can also be set using the
`nargs` option. This allows for a specific or fluid number of options to
be passed into a single cli option.

The `nargs` option passes through to `argparse`, if the examples in this
document do not make sense you can also check the `argsparse` section on
`nargs`.

Integer (1)
-----------

Specifying an integer defines the explicit number of options to require:

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "nargs": 3,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

The above example will require that exactly 3 options are passed to `--test`.

Question Mark (?)
-----------------

One argument will be consumed from the command line if possible, and produced
as a single item. If no command-line argument is present, the value from
default will be produced.

Asterisk (*)
------------

All command-line arguments present are gathered into a list.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "nargs": "*",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Plus (+)
--------
Just like '*', all command-line args present are gathered into a list.
Additionally, an error message will be generated if there wasn't at
least one command-line argument present.

Type
====

The value type can be enforced with the `type` option. A type can
be passed in that will be enforced, such as `int` or `str`.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "type": int,
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

Render
======

Sometimes it is desirable to load up complex data structures from the
command line. This can be done with the `render` option. The `render`
option allows you to specify that the argument passed will be rendered
using a data serialization medium such as json or yaml.

.. code-block:: python

    CLI_CONFIG = {
        "test": {
            "render": "yaml",
        },
    }

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This cli could then look like this:

.. code-block:: bash

    myapp --test "Food: true"

Then the resulting value would be: `{"Food": True}`


Subcommands
===========

Sometimes it is desirable to have subcommands. Subcommands allow your CLI
to work in a way similar to the git cli, where you have multiple routines
that all can be called from a single command.

This example shows how multiple subcommands can be defined and utilized.

.. code-block:: python

    CLI_CONFIG = {
        "name": {
            "subcommands": ["test", "apply"],
        },
        "weight": {},
        "power": {
            "subcommands": ["apply"],
        },
    }
    CONFIG = {
        "name": {
            "default": "frank",
            "help": "Enter the name to use",
        },
        "weight": {
            "default": "150",
            "help": "Enter how heavy it should be",
        },
        "power": {
            "default": "100",
            "help": "Enter how powerful it should be",
        },
    }

    SUBCOMMANDS = {
        "test": {
            "help": "Used to test",
            "desc": "When running in test mode, things will be tested",
        },
        "apply": {
            "help": "Used to apply",
            "desc": "When running in apply mode, things will be applied",
        },
    }


In this example we see that the option `name` will be available under
the subcommands `test` and `apply`. The option `power` will be available
only under the subcommand `apply` and the option `weight` is globally
available.

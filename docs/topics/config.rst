===============
CONFIG Settings
===============

The *CONFIG* dictionary is a very powerful system for applying configuration
values and where the root of configuration should be applied. The *CONFIG*
dictionary is primarily used for non command line flags, those should be
reserved for the *CLI_CONFIG* dictionary.

Basic Settings
==============

Nearly every config setting needs to have 2 basic options, `default` and
`help`. These are very self explanatory, `default` sets the default value
of the option if no option is passed and `help` presents, not only the
command line help, but is also the single source of documentation for the
option.

Here is a simple example:

.. code-block:: python

    CONFIG = {
        "test": {
            "default": "Red",
            "help": "What color to test",
        },
    }

This establishes the basic data for the setting and is all that is needed for
settings in the *CONFIG* dictionary.

Destination
===========

When the argument is named "test" it will appear on the option namespace as
"test". This may not always be desirable. If the name of the option and where
it needs to be stored differs, then use the `dest` option:

.. code-block:: python

    CONFIG = {
        "test": {
            "default": "Red",
            "dest": "cheese",
            "help": "What color to test",
        },
    }

In this example the option will be stored under the name "cheese".

Location
========

Once the config system has been run, all configuration data will appear in the
`hub.OPT` namespace. This means that in our above example, if the system in
question is part of an app named `myapp`, then the option data will be present
at `hub.OPT["myapp"]["test"]`.

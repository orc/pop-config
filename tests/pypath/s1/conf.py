CLI_CONFIG = {
    "single": {"subcommands": ["test"],},
    "double": {"subcommands": ["test", "valid"],},
}

CONFIG = {
    "single": {"help": "Single option on just test", "default": "one",},
    "double": {"help": "Double command on test and valid", "default": "two",},
}

SUBCOMMANDS = {
    "test": {"help": "Some Help", "desc": "A description",},
    "valid": {"help": "Validate Help", "desc": "Validate desc",},
}

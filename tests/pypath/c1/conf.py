CLI_CONFIG = {
    "test": {},
    "osvar": {},
    "act": {"options": ["-A"], "action": "store_true",},
    "int": {},
    "string": {},
}
CONFIG = {
    "test": {"default": 47, "help": "Some Help MSG",},
    "foo": {"default": "def", "os": "footest", "help": "Help"},
    "osvar": {"default": "not me", "os": "POPTESTOSVAR", "help": "Help"},
    "lastos": {"default": "no os", "os": "lastos", "help": "Help"},
    "act": {"default": False, "help": "Some Help",},
    "int": {"default": 2016, "help": "An int type", "type": int},
    "string": {"default": "cheese", "help": "An int type", "type": str},
}
SUBCOMMANDS = {}
DYNE = {"c1": ["c1"]}

CLI_CONFIG = {
    "first": {"positional": True, "display_priority": 1,},
    "second": {"positional": True, "display_priority": 2,},
}

CONFIG = {
    "first": {"default": "one", "help": "some help",},
    "second": {"default": "two", "help": "some help",},
}

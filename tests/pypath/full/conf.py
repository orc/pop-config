CLI_CONFIG = {
    "config": {},
    "oscli": {},
    "alls": {},
}

CONFIG = {
    "config": {"default": "/etc/config", "help": "The config file",},
    "osalone": {
        "default": "default",
        "os": "OS_ALONE",
        "help": "An os var able option, but not a cli option",
    },
    "oscli": {
        "default": "default",
        "os": "OS_CLI",
        "help": "An os var able option, and cli option",
    },
    "alls": {
        "default": "default",
        "os": "ALLS",
        "help": "An option that can be hit from all angles",
    },
}

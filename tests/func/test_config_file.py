# Import python libs
import os
import sys

# Import pop libs
import pop.hub

# Import third aprty libs
import pytest

CPATH_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), "config")


def test_file():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "simple.conf"))
    hub.config.integrate.load(["f1"], "f1")
    assert hub.OPT["f1"]["test"] == "simple"
    assert hub.OPT["f1"]["error"] == False


def test_file_default():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load(["f3"], "f3")
    assert hub.OPT["f3"]["cheese"] == True


def test_dir():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config-dir")
    sys.argv.append(os.path.join(CPATH_DIR, "simple.d"))
    hub.config.integrate.load(["f2"], "f2")
    assert hub.OPT["f2"]["first"] == True
    assert hub.OPT["f2"]["second"] == True
    assert hub.OPT["f2"]["rocks"] == True
    assert hub.OPT["f2"]["seeds"] == True


def test_includes():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "include1.conf"))
    hub.config.integrate.load("f1", "f1")
    assert hub.OPT["f1"]["included"] == True
    assert hub.OPT["f1"]["test"] == True


def test_roots():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load(["r1"], "r1")
    assert hub.OPT["r1"]["cheese_dir"].endswith(".r1/etc/cheese")
    assert hub.OPT["r1"]["bacon_dir"].endswith(".r1/etc/foo/bacon")

# Import python libs
import os
import sys

# Import pop libs
import pop.hub

# Import third aprty libs
import pytest

# Things left to test:
# Full ordered tests, with all attributes on multiple layers
# CLI arg Groups


def test_source():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load(["m1", "m2"], "m1")
    assert hub.OPT["m2"]["base"] == "single"


def test_load():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load("c1", "c1")
    assert hub.OPT["c1"]["int"] == 2016
    assert hub.OPT["c1"]["string"] == "cheese"
    assert hub.OPT["c1"]["test"] == 47


def test_render_noargs():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load("rend1", "rend1")
    assert hub.OPT["rend1"]["yaml"] == {"rubber": "Svien"}
    assert hub.OPT["rend1"]["json"] == {"rubber": "Svien"}


def test_render_args():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--yaml")
    sys.argv.append("test: True")
    sys.argv.append("--json")
    sys.argv.append('{"test": true}')
    hub.config.integrate.load("rend1", "rend1")
    import pprint

    pprint.pprint(hub.OPT)
    assert hub.OPT["rend1"]["yaml"] == {"test": True}
    assert hub.OPT["rend1"]["json"] == {"test": True}


def test_os():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    os.environ["FOOTEST"] = "osvar"
    os.environ["POPTESTOSVAR"] = "good"
    hub.config.integrate.load("c1", "c1")
    # Lowercase works
    assert hub.OPT["c1"]["foo"] == "osvar"
    # Standard works with cli enabled but not called
    assert hub.OPT["c1"]["osvar"] == "good"
    # No OSVAR passed
    assert hub.OPT["c1"]["lastos"] == "no os"
    os.environ.pop("FOOTEST")
    os.environ.pop("POPTESTOSVAR")


def test_log():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    hub.config.integrate.load("c1", "c1")
    assert hub.OPT["c1"]["log_file"]


def test_args():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--test")
    sys.argv.append("new")
    hub.config.integrate.load("c1", "c1")
    assert hub.OPT["c1"]["test"] == "new"


def test_no_args():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--foo")
    sys.argv.append("new")
    with pytest.raises(SystemExit):
        hub.config.integrate.load("c1", "c1")


def test_actions():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("-A")
    hub.config.integrate.load("c1", "c1")
    assert hub.OPT["c1"]["act"] is True


def test_cli_over_os():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    os.environ["FOOTEST"] = "osvar"
    os.environ["POPTESTOSVAR"] = "good"
    sys.argv.append("--osvar")
    sys.argv.append("new")
    hub.config.integrate.load("c1", "c1")
    # Lowercase works
    assert hub.OPT["c1"]["foo"] == "osvar"
    # Standard works
    assert hub.OPT["c1"]["osvar"] == "new"
    # No OSVAR passed
    assert hub.OPT["c1"]["lastos"] == "no os"
    os.environ.pop("FOOTEST")
    os.environ.pop("POPTESTOSVAR")


def test_dyne_cli():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--dyned1")
    sys.argv.append("DYNED1")
    sys.argv.append("--exdyned1")
    sys.argv.append("EXDYNED1")
    sys.argv.append("--exdyned2")
    sys.argv.append("EXDYNED2")
    sys.argv.append("--exdyned3")
    sys.argv.append("EXDYNED3")
    hub.config.integrate.load("d1", "d1")
    assert hub.OPT["d1"]["dyned1"] == "DYNED1"
    assert hub.OPT["d1"]["exdyned1"] == "EXDYNED1"
    assert hub.OPT["d1"]["exdyned2"] == "EXDYNED2"
    assert hub.OPT["d1"]["exdyned3"] == "EXDYNED3"


def test_subcommands_single():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("test")
    sys.argv.append("--single")
    sys.argv.append("another!")
    hub.config.integrate.load("s1", "s1")
    assert hub.OPT["s1"]["single"] == "another!"


def test_subcommands_double_1():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("test")
    sys.argv.append("--double")
    sys.argv.append("another!")
    hub.config.integrate.load("s1", "s1")
    assert hub.OPT["s1"]["double"] == "another!"


def test_subcommands_double_2():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("valid")
    sys.argv.append("--single")
    sys.argv.append("another!")
    with pytest.raises(SystemExit):
        hub.config.integrate.load("s1", "s1")


def test_subcommands_invalid():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("test")
    sys.argv.append("--double")
    sys.argv.append("another!")
    hub.config.integrate.load("s1", "s1")
    assert hub.OPT["s1"]["double"] == "another!"


def test_positional():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("foo")
    sys.argv.append("bar")
    hub.config.integrate.load("p1", "p1")
    assert hub.OPT["p1"]["first"] == "foo"
    assert hub.OPT["p1"]["second"] == "bar"


def test_version():
    hub = pop.hub.Hub()
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--version")
    with pytest.raises(SystemExit):
        hub.config.integrate.load("c1", "c1")

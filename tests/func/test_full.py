# Import python libs
import os
import sys

# Import pop libs
import pop.hub

# Import third aprty libs
import pytest

CPATH_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), "config")


def test_ordering_1():
    hub = pop.hub.Hub()
    os.environ["OS_ALONE"] = "os"
    os.environ["OS_CLI"] = "os"
    os.environ["ALLS"] = "os"
    hub.pop.sub.add("pop_config.config")
    sys.argv.append("--config")
    sys.argv.append(os.path.join(CPATH_DIR, "full.conf"))
    sys.argv.append("--alls")
    sys.argv.append("cli")
    sys.argv.append("--oscli")
    sys.argv.append("cli")
    sys.argv.append("--exdyned1")
    sys.argv.append("EXDYNED1")
    sys.argv.append("--exdyned2")
    sys.argv.append("EXDYNED2")
    sys.argv.append("--exdyned3")
    sys.argv.append("EXDYNED3")
    hub.config.integrate.load(["full", "d1", "m1", "m2"], "full", ["d1"])
    assert hub.OPT["d1"]["exdyned1"] == "EXDYNED1"
    assert hub.OPT["d1"]["exdyned2"] == "EXDYNED2"
    assert hub.OPT["d1"]["exdyned3"] == "EXDYNED3"
    assert hub.OPT["m2"]["base"] == "single"
    assert hub.OPT["full"]["alls"] == "cli"
    assert hub.OPT["full"]["oscli"] == "cli"
    assert hub.OPT["full"]["osalone"] == "os"
